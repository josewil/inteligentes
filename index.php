
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="boostrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">


    <title>Document</title>
</head>
<body>
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#"><img src="img/hipermaxi.png" alt="logo" width="150"></a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio</a>
                </li>
            
        
				
				<li class="nav-item">
                    <a class="nav-link font-weight-bold" href="salir.php">Salir</a>
                </li>
                </ul>
            </div>
        </nav>
	</div>
	
	   <div class="row">
			<div class="col-md-8">
			<div class="wrap" id="contenedor">
		<h1>Escoge un producto</h1>
		<div class="store-wrapper">
			<div class="category_list">
			<a href="#" class="category_item" category="all">Todo</a>
				<a href="#" class="category_item" category="carnes">Carnes y Embutidos</a>
				<a href="#" class="category_item" category="Frutas">Frutas y Verduras</a>
				<a href="#" class="category_item" category="panaderia">Panaderia y Reposteria</a>
				<a href="#" class="category_item" category="huevo">huevo lacteos Cafe</a>
				<a href="#" class="category_item" category="aceite">Aceites y Pastas</a>
			</div>
			<section class="products-list">
			<?php
                          require("conexion.php");
                         
                          $consulta = "SELECT * FROM producto";
                  		        if($resultado = $enlace->query($consulta)) {
                            $c=1;
                            while($row = $resultado->fetch_array()) {?>
                            <div class="product-item" category="<?php echo $row['categoria_producto'];?>" >
					<img  src="<?php echo$row['imagen_producto'];?> "width="100" height="300" alt="" >
					<p class="text-center">500 Grm.
					<?php echo$row['precio_producto']."Bs.";?><br>
					<?php echo $row["nombre_producto"];?></p>
					<form action="" method="post">
			
						<label for="cantidad">Cantidad</label>
						<input type="number" id="cantidad" name="cantidad" style="max-width:80px;" min="1">
						
					</form>
					<button type="submit" >Añadir</button>
					
				</div>
                          <?php
                             }
                             $resultado->close();
                           }
                          ?>
			
				
				
			</section>
		</div>
	</div>
			</div>
			
   
    <script type="text/javascript" src="js/jquery-1.4.2.js" ></script>
	<script type="text/javascript" src="js/cufon-yui.js"></script>
	<script type="text/javascript" src="js/cufon-replace.js"></script>
	<script type="text/javascript" src="js/Avenir_900.font.js"></script>
	<script type="text/javascript" src="js/Avenir_300.font.js"></script>
    <script type="text/javascript" src="js/Avenir_500.font.js"></script>
    <script src="js/javascrip.js"></script>
</body>
</html>
